import React, {Component} from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class Post extends Component {
  static defaultProps = {
    title: "Aprendendo React Native",
  };
  render() {
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{ this.props.title }</Text>
       <Text style={styles.autor}>Diego Schell Fernandes</Text>
       <Text style={styles.content}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  box: {
    backgroundColor: '#FFF',
    padding:20,
    margin: 20,
    borderRadius: 5,
    shadowColor: '#DA6C6C',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    shadowOffset: {
      height: 0,
      width: 2
    },
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  autor: {
    fontSize:12,
    color: '#999',

    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  content: {
    color: '#666',
  }
});
