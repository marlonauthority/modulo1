import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';

import 'config/ReactotronConfig';
import 'config/DevToolsConfig';

import Post from './components/Post';

export default class App extends Component {

  render() {
    return (
     <View style={styles.container} >
        <Text style={styles.top}>GoNative App</Text>
          <ScrollView>
            <Post />
            <Post />
            <Post />
            <Post />
            <Post />
            <Post />
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  top: {
    backgroundColor: '#FFF',
    padding:20,
    margin: 20,
    fontSize: 16,
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EE7777',
  }
});
