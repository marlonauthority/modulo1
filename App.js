import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class App extends Component {
    componentDidMount(){
        setTimeout(() => {
            this.setState({ text: 'Hello World!' })
        }, 3000)
    }

    render() {
        return <View style={ styles.container } />;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#CCC"
    }
});